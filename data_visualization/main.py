# Script for data visualization and plotting
# Author: Matt Rochford (mrochfor@ucsc.edu)

# Import packages
import pandas as pd
import numpy as np
import os
import glob
import matplotlib.pyplot as plt
import seaborn as sns
import time 


def main(plate_reader_file,pcr_file):
    """
    Main script for running entire data visualization

    Inputs:
    plate_reader_file = file name for plate reader data
    pcr_file = file name for pcr data
    """

    # Load plate reader csv file
    df_plate_reader = load_plate_reader_data(plate_reader_file)

    #Load pcr data
    df_pcr = load_pcr_data(pcr_file)

    # Handle well label template generation and loading
    try:
        df_labels_plate_reader, df_labels_pcr = well_template_generation_and_loading(df_plate_reader,df_pcr)
        print('Labels files have been loaded')

    except:
        quit()

    # Create new directory for saving plots
    create_plots_directory()

    # Perform calculations on PCR data and make plots
    df_pcr_samples, df_pcr_sets = pcr_calculations(df_pcr,df_labels_pcr)
    make_plots_pcr(df_pcr_samples,df_pcr_sets)

    # Perform calculations on plate reader data and make plots
    df_plate_reader_samples, df_plate_reader_sets = plate_reader_calculations(df_plate_reader,df_labels_plate_reader)
    #make_plots_plate_reader(df_pcr_samples,df_pcr_sets)

def plate_reader_calculations(df,labels):
    """
    This function calculates new columns to use for plotting

    Inputs:
    df = dataframe loaded from the plate reader csv

    Outputs:
    df = plate reader dataframe with the new calculated columns added
    """

    ### Sample level calculations

    ## Zero correct values at the sample level
    for c in df.columns:

        # If a well column
        if len(c) < 4:

            baseline = df.loc[df['minutes'] == 0][c]

            # Calculate seconds and minutes columns
            df[c+'_ZC'] = df.apply (lambda row: zero_correct_column(row,c,baseline), axis=1)

    ## Calculate end point slope for each zero corrected sample
    for c in df.columns:

        # If a well column
        if '_ZC' in c:

            first_point = df.loc[df['minutes'] == 0][c]

            # Calculate seconds and minutes columns
            df[c+'_endpoint_slope'] = df.apply (lambda row: calculate_endpoint_slope(row,c,first_point), axis=1)

    ## Calculate point slope for each sample
    for c in df.columns:

        # If a well column
        if '_ZC' in c and '_endpoint' not in c:

            # Initialize slope list with first value equal to 0
            slopes = [0]

            # Loop through each row and calculate slope
            for i in range(1,df.shape[0]):

                slope = (df.iloc[i][c] - df.iloc[i-1][c])/(df.iloc[i]['minutes'] - df.iloc[i-1]['minutes'])
                slopes.append(slope)

            df[c+'_point_slope'] = slopes

    # Replace NaN values with 0
    df = df.fillna(0)

    ### Replicate level calculations

    # Create new df to store sample level data
    df_samples = df.copy()

    # Loop through each replicate set and apply calculations
    for label in labels.label.unique():

        # Get wells for replicate set
        wells = labels.loc[labels['label'] == label, 'well'].tolist()

        # Calculate mean
        df[label] = df.apply (lambda row: calculate_replicate_mean_zc(row,wells), axis=1)

        # Calculate standard deviation
        df[label+' standard deviation'] = df.apply (lambda row: calculate_replicate_std_zc(row,wells), axis=1)

        # Get first point (should always be zero this is probably unneccessary)
        first_point = df.loc[df['minutes'] == 0][label]

        # Calculate endpoint slope
        df[label+' endpoint slope'] = df.apply (lambda row: calculate_endpoint_slope(row,label,first_point), axis=1)

        ## Calculate point slope
                  
        # Initialize slope list with first value equal to 0
        slopes = [0]

        # Loop through each row and calculate slope
        for i in range(1,df.shape[0]):

            slope = (df.iloc[i][label] - df.iloc[i-1][label])/(df.iloc[i]['minutes'] - df.iloc[i-1]['minutes'])
            slopes.append(slope)

        # Assign calculated slopes to dataframe column
        df[label+' point slope'] = slopes

        # Remove sample level columns for this label
        for well in wells:

            df = df[df.columns.drop(list(df.filter(regex=well)))]

    # Replace NaN values with 0
    df = df.fillna(0)

    return df_samples,df


def make_plots_plate_reader(df,sets=None):

    ### 1. point plot - Fluorescence vs time - with error bars being controlable confidence interval (i sent you this already) - option to take every X data point from set (sometimes may only wanna graph like every third for example) ###

    plt.errorbar(df['minutes'], df['High_mean'], df['High_std'], linestyle='None', marker='^',label='High')
    plt.errorbar(df['minutes'], df['Medium_mean'], df['Medium_std'], linestyle='None', marker='^',label='Medium')
    plt.errorbar(df['minutes'], df['Low_mean'], df['Low_std'], linestyle='None', marker='^',label='Low')
    plt.errorbar(df['minutes'], df['Negative_mean'], df['Negative_std'], linestyle='None', marker='^',label='Negative')
    plt.legend()
    plt.xlabel('Minutes')
    plt.ylabel('Fluorescence')
    plt.show()

    plt.errorbar(df['minutes'], df['High_mean_zc'], df['High_std_zc'], linestyle='None', marker='^',label='High')
    plt.errorbar(df['minutes'], df['Medium_mean_zc'], df['Medium_std_zc'], linestyle='None', marker='^',label='Medium')
    plt.errorbar(df['minutes'], df['Low_mean_zc'], df['Low_std_zc'], linestyle='None', marker='^',label='Low')
    plt.errorbar(df['minutes'], df['Negative_mean_zc'], df['Negative_std_zc'], linestyle='None', marker='^',label='Negative')
    plt.legend()
    plt.xlabel('Minutes')
    plt.ylabel('Fluorescence (Zero Corrected)')
    plt.show()


    ### 2. point plot - endpoint slope vs end time - controllable: confidence interval (on error bars), end time ###

    plt.errorbar(df['minutes'], df['High_mean_es'], df['High_std_es'], linestyle='None', marker='^',label='High')
    plt.errorbar(df['minutes'], df['Medium_mean_es'], df['Medium_std_es'], linestyle='None', marker='^',label='Medium')
    plt.errorbar(df['minutes'], df['Low_mean_es'], df['Low_std_es'], linestyle='None', marker='^',label='Low')
    plt.errorbar(df['minutes'], df['Negative_mean_es'], df['Negative_std_es'], linestyle='None', marker='^',label='Negative')
    plt.legend()
    plt.xlabel('Minutes')
    plt.ylabel('End Point Slope')
    plt.show()

    ### 5. point plot - point slope vs time - controllable: confidence interval (on error bars), end time ###

    plt.errorbar(df['minutes'], df['High_mean_ps'], df['High_std_ps'], linestyle='None', marker='^',label='High')
    plt.errorbar(df['minutes'], df['Medium_mean_ps'], df['Medium_std_ps'], linestyle='None', marker='^',label='Medium')
    plt.errorbar(df['minutes'], df['Low_mean_ps'], df['Low_std_ps'], linestyle='None', marker='^',label='Low')
    plt.errorbar(df['minutes'], df['Negative_mean_ps'], df['Negative_std_ps'], linestyle='None', marker='^',label='Negative')
    plt.legend()
    plt.xlabel('Minutes')
    plt.ylabel('Point Slope')
    plt.show()

    ########


    # plt.scatter(df['minutes'], df['L1'])
    # plt.show()

    # plt.scatter(df['minutes'], df['L1_zero_corrected'])
    # plt.show()

    # plt.scatter(df['minutes'], df['L1_endpoint_slope'])
    # plt.show()

    # plt.scatter(df['minutes'], df['L1_point_slope'])
    # plt.show()


def pcr_calculations(df,labels):
    """
    This function calculates new columns to use for plotting

    Inputs:
    df = dataframe loaded from the pcr excel sheet

    Outputs:
    df = pcr dataframe with the new calculated columns added
    """

    # Get list of wells, labels, and cycles
    wells = df['Well Position'].unique()
    label_list = labels['label'].unique()
    cycles = df['Cycle'].unique()

    # Calculate zero correct values at the sample level
    for well in wells:

        baseline = df.loc[(df['Cycle'] == 1) & (df['Well Position'] == well)]['SYBR'].item()

        df.loc[df['Well Position'] == well, 'SYBR_zc'] = df['SYBR'] - baseline

    # Create replicate set columns
    data_list = []
    for label in label_list:

        # Get relevant wells
        wells = labels.loc[labels['label'] == label]['well'].tolist()

        # Get replicate subset
        for cycle in cycles:

            # Get relevant values
            values = df[(df['Cycle'] == cycle) & (df['Well Position'].isin(wells))].SYBR_zc

            # Calculate average and sd
            data_row = [label,cycle,values.mean(),values.std()]

            data_list.append(data_row)  

    # Convert data into dataframe format
    df_sets = pd.DataFrame(data_list, columns=['label','cycle','sybr_zc_avg','sybr_zc_sd'])

    # Join labels to sample level dataframe
    df = pd.merge(df,labels, left_on='Well Position', right_on='well', how='left').drop('well',axis=1)

    return df, df_sets


def make_plots_pcr(df_samples,df_sets):
    """
    This function handles all the plotting for the PCR dataset

    Inputs:
    df_samples = PCR data at the sample level
    df_sets = PCR data at the replicate level

    Outptus:
    None (plots saved to /plots/ directory)
    """

    ## CYCLES TO THRESHOLD CALCULATIONS

    # Find threshold cutoff value (20% of max SYBR)
    threshold = df_samples['SYBR_zc'].max()*0.2

    # Get range of cycles
    c1 = df_samples['Cycle'].min()
    c2 = df_samples['Cycle'].max()


    # Calculate CT per sample
    for well in df_samples['Well Position'].unique():

        for cycle in range(c1,c2+1):
            
            # Check if value is above threshold
            if df_samples.loc[(df_samples['Cycle'] == cycle) & (df_samples['Well Position'] == well)]['SYBR_zc'].item() > threshold:
                
                # Update well name with CT value
                df_samples.loc[df_samples["Well Position"] == well, "Well Position"] = well + f' (CT = {cycle})'

                # Skip rest of loop
                break

            if cycle == c2:
                df_samples.loc[df_samples["Well Position"] == well, "Well Position"] = well + f' (CT = N/A)'

    # Calculate CTs for replicates
    for label in df_sets['label'].unique():
        
        # Find relevant wells for replicate set
        subset = df_samples.loc[df_samples['label']==label]

        # Get unique list of wells
        wells = subset["Well Position"].unique()

        # Parse CT values out of each string
        CTs = []
        for well in wells:
            ct = well.split('= ')[-1][:-1]

            try:
                CTs.append(int(ct))

            except:
                CTs.append(None)

        # Convert to numpy to take mean and standard deviation
        arr = np.array(CTs)

        # Add CT Value to label in dataframe
        try:
            m = round(arr.mean(),2)
            sd = round(arr.std(),2)

            df_sets.loc[df_sets["label"] == label, "label"] = label + f' (CT = {m}, SD = {sd})'
        
        except:
            df_sets.loc[df_sets["label"] == label, "label"] = label + f' (CT = N/A, SD = {sd})'

    ## Sample level plotting
    plot = sns.lineplot(data=df_samples, x="Cycle", y="SYBR_zc", hue="Well Position",palette='tab20c')
    plot.axhline(y=threshold, color='r', linestyle='--', label='CT cutoff')
    plot.legend(bbox_to_anchor=(1.04, 1), loc="upper left", prop={'size': 6})
    plot.set_xlabel('Cycles')
    plot.set_ylabel('SYBR (Zero Corrected) (AU)')
    plot.set_title('Sample Level Fluorescence Over Time')
    plot.grid()

    # Save plot to file
    plot = plot.get_figure()
    plot.savefig('plots/zero_corrected_sample_level.png',bbox_inches='tight')

    # Clear figure from memory
    plot.clf()

    ## Replicate level plotting
    plot = sns.lineplot(data=df_sets, x="cycle", y="sybr_zc_avg", hue="label",palette='tab20c')
    plot.axhline(y=threshold, color='r', linestyle='--', label='CT cutoff')
    plot.legend(bbox_to_anchor=(1.04, 1), loc="upper left", prop={'size': 6})
    plot.set_xlabel('Cycles')
    plot.set_ylabel('SYBR (Zero Corrected) (AU)')
    plot.set_title('Replicate Level Fluorescence Over Time')
    plot.grid()

    # Save plot to file
    plot = plot.get_figure()
    plot.savefig('plots/zero_corrected_replicate_level.png',bbox_inches='tight')
    
    # Clear figure from memory
    plot.clf()

    # Create replicate level plot with error bars
    for label in df_sets['label'].unique():
        df_subset = df_sets.loc[df_sets['label']==label]
        plt.errorbar(df_subset['cycle'], df_subset['sybr_zc_avg'], df_subset['sybr_zc_sd'], linestyle='None', marker='^',label=label)

    plt.axhline(y=threshold, color='r', linestyle='--', label='CT cutoff')
    plt.legend(bbox_to_anchor=(1.04, 1), loc="upper left", prop={'size': 6})
    plt.xlabel('Cycles')
    plt.ylabel('SYBR (Zero Corrected) (AU)')
    plt.title('Replicate Level Fluorescence Over Time')
    plt.grid()

    # Save plot to file
    plt.savefig('plots/zero_corrected_replicate_level_with_ci.png',bbox_inches='tight')


def load_pcr_data(file,sheet_name='Multicomponent Data'):
    """
    Load the pcr excel sheet data into a dataframe

    Inputs:
    file = file name for the excel file
    sheet_name = name of sheet that contains the PCR SYBR data

    Outputs:
    df = dataframe of the readings
    """

    # Read excel sheet using pandas
    data = pd.read_excel(file, sheet_name=sheet_name)
    #data = data.reset_index(drop=True)

    # Save dataframe as temporary csv
    data.to_csv('temp.csv',index=False)

    try:
        # Now read temporary csv file line by line
        with open('temp.csv','rb') as f:

            # Create list of lists for each line
            lines = [line.strip().split(b',') for line in f] 

            # Set data flag to false and initialize list to store data
            data_flag = False
            data_list = []

            # Loop through lines
            for i, x in enumerate(lines): 

                # Store data if flag is set
                if data_flag:

                    # Check if well has data in it
                    if x[-1] != b'':

                        data =[t.decode("utf-8") for t in x]
                        data_list.append(data)

                # Check for lines that include the start id             
                if x[0] == b'Well':
                    
                    # Toggle data flag
                    data_flag = True

                    # Get names of columns
                    columns = [t.decode("utf-8") for t in x]

    # In case of failure remove temp file and print error message
    except Exception as e:

        # Remove temporary file
        os.remove('temp.csv')

        # Print error and quit program
        print(e)
        quit()

    # Remove temporary file
    os.remove('temp.csv')

    # Convert data into dataframe format
    df = pd.DataFrame(data_list, columns=columns)

    # Remove leading whitespace for Well Position column
    df[columns[1]] = df[columns[1]].apply(lambda r: r.lstrip())

    # Convert column data types to numeric values
    df = df.astype({"Cycle": int, "SYBR": float})

    return df


def generate_label_csv(df,label):
    """
    Function to generate an empty csv file to add labels for each well

    Inputs:
    df = the dataframe generated from the plate reader data
    label = 'plate_reader' or 'pcr'

    Outputs:
    none (csv file is created in the current directory to be filled out)
    """

    # Make sure label is a valid value
    if label != 'plate_reader' and label != 'pcr':
        print("Label must be 'plate_reader' or 'pcr'")
        raise ValueError

    if label == 'plate_reader':

        # Initialize empty list to store well names
        wells = []

        # Loop through columns in plate reader dataframe and add well names to list
        for c in df.columns:
            if len(c) < 4:

                wells.append(c)

        # Create one column dataframe of the wells
        df = pd.DataFrame({'well':wells})

        # Add other columns to dataframe
        df['replicate_set'] = ''
        df['concentration'] = ''
        df['label'] = ''

    elif label == 'pcr':

        # Initialize empty list to store well names
        wells = df['Well Position'].unique()

        # Create one column dataframe of the wells
        df = pd.DataFrame({'well':wells})

        # Add other columns to dataframe
        df['replicate_set'] = ''
        df['concentration'] = ''
        df['label'] = ''

    # Make file name
    file_name = 'well_labels_'+label+'.csv'

    # Save dataframe as csv file
    df.to_csv(file_name, index=False)

    print(f'New blank {label} well labels csv file has been generated')


def load_plate_reader_data(file):
    """
    Load from the plate reader csv into a dataframe

    Inputs:
    file = file name for the csv

    Outputs:
    df = dataframe of the readings
    """

    # Read file line by line to determine number of lines to skip
    with open(file,'rb') as f:

        # Create list of lists for each line
        lines = [line.split() for line in f] 

        # Set data flag to false and initialize list to store data
        data_flag = False
        data_list = []

        # Loop through lines
        for i, x in enumerate(lines): 

            # Store data if flag is set
            if data_flag:
                
                # Check if line is empty to control data flag toggle
                if len(x) == 0:
                    data_flag = False

                # Otherwise transform data and store
                else:

                    data = x[0].decode("utf-8").split(',')
                    data_list.append(data)

            # Check for lines that include the start id
            try:                
                if x[0] == b'Time,T\xb0':
                    
                    # Toggle data flag
                    data_flag = True

                    # Get names of columns
                    columns = x[-1].decode("utf-8").split(',')
                    columns[0] = 'temperature'
                    columns.insert(0,'time') # Hard coding the first value because of degree symbol causing unicode weirdness
                    
            except:
                pass

    # Convert data into dataframe format
    df = pd.DataFrame(data_list, columns=columns[:-1])

    # Calculate seconds and minutes columns
    df['seconds'] = df.apply (lambda row: calculate_secs(row), axis=1)
    df['minutes'] = df.apply(lambda row: row.seconds/60, axis=1)

    # Remove rows from other project 
    df = df.drop(df[df.seconds%60 != 0].index)

    # Remove columns without data
    for c in df.columns:

        if len(c) < 4:

            # Check if there is data in any of the columns
            if not df[c].sum():

                df = df.drop(c, axis=1)

            # Check if data is real or just noise
            else:
                df[c] = df[c].astype(int)

                if df[c].max() < 50:
                    df = df.drop(c, axis=1)

    # Convert to numeric data types
    df['temperature'] = df['temperature'].astype(float)

    # for c in columns[2:]:
    #     try:
    #         df[c] = df[c].astype(int)
    #     except:
    #         pass

    # Remove 3rd column manually
    #df = df.drop(df.columns[2], axis=1)

    return df


def create_plots_directory():
    """
    This function will create a new directory to save plots to.
    If directory already exists it will empty the directory

    Inputs: 
    None

    Outputs: 
    None
    """
    
    try:
        os.mkdir('plots')

    except:
        files = glob.glob('plots/*')
        for f in files:
            os.remove(f)


def well_template_generation_and_loading(df_plate_reader,df_pcr):
    """
    This function handles everything to do with generating well template csvs and loading them

    Inputs:
    df_plate_reader = dataframe of plate reader data
    df_pcr = dataframe of pcr data

    Outputs:
    df_labels_plate_reader = dataframe of well labels for the plate reader data
    df_labels_pcr = dataframe of well labels for the plate reader data
    """

    # Check for filled out label csv's
    pr = False
    pcr = False
    print('Checking for filled out label csv files')
    try:
        df_labels_plate_reader = pd.read_csv('well_labels_plate_reader.csv')
        pr = True

    except:
        print('Plate reader well labels file does not exist')
        generate_label_csv(df_plate_reader,label='plate_reader')

    try:
        df_labels_pcr = pd.read_csv('well_labels_pcr.csv')
        pcr = True

    except:
        print('PCR well labels file does not exist')
        generate_label_csv(df_pcr,label='pcr')

    if not (pr and pcr):
        print('Please fill out new template file(s) and rerun script (make sure all label cells have been filled)')
        quit()

    # Check if each template csv has actually been filled out
    if df_labels_plate_reader.isnull()['label'].iloc[-1]:
        print('Please finish filling out the plate reader well labels and rerun script (make sure all label cells have been filled)')
        pr = False

    #print(df_labels_pcr['label'].iloc[-1])
    if df_labels_pcr.isnull()['label'].iloc[-1]:
        print('Please finish filling out the pcr well labels and rerun script (make sure all label cells have been filled)')
        pcr = False

    if pr and pcr:
        return df_labels_plate_reader, df_labels_pcr


def calculate_secs(df):
    """
    Helper function for converting time column into seconds
    """
    return int(df['time'].split(':')[0])*60*60+int(df['time'].split(':')[1])*60+int(df['time'].split(':')[2])


def zero_correct_column(df,column,baseline):
    """
    Helper function so that we can use the df.apply function when zero correcting the fluorescence values"
    """
    return df[column] - baseline


def calculate_endpoint_slope(df,column,first_point):
    """
    Helper function to calculate endpoint slope for each sample"
    """
    return (df[column] - first_point)/df['minutes']

def calculate_replicate_mean(df,wells):
    """
    Helper function to calculate mean for entire replicate set"
    """
    return df[wells].mean(axis=0)

def calculate_replicate_std(df,wells):
    """
    Helper function to calculate standard deviation for entire replicate set"
    """
    return df[wells].std(axis=0)

def calculate_replicate_mean_zc(df,wells):
    """
    Helper function to calculate mean for entire replicate set"
    """
    wells = [s + '_ZC' for s in wells]
    return df[wells].mean(axis=0)

def calculate_replicate_std_zc(df,wells):
    """
    Helper function to calculate standard deviation for entire replicate set"
    """
    wells = [s + '_ZC' for s in wells]
    return df[wells].std(axis=0)

def calculate_replicate_mean_es(df,wells):
    """
    Helper function to calculate mean for entire replicate set"
    """
    wells = [s + '_endpoint_slope' for s in wells]
    return df[wells].mean(axis=0)

def calculate_replicate_std_es(df,wells):
    """
    Helper function to calculate standard deviation for entire replicate set"
    """
    wells = [s + '_endpoint_slope' for s in wells]
    return df[wells].std(axis=0)

def calculate_replicate_mean_ps(df,wells):
    """
    Helper function to calculate mean for entire replicate set"
    """
    wells = [s + '_point_slope' for s in wells]
    return df[wells].mean(axis=0)

def calculate_replicate_std_ps(df,wells):
    """
    Helper function to calculate standard deviation for entire replicate set"
    """
    wells = [s + '_point_slope' for s in wells]
    return df[wells].std(axis=0)




if __name__ == '__main__':
    file1 = 'data/10052023_nis20_v2.csv'
    file2 = 'data/20042023_gRNA_delayfullplate.csv'
    file3 = 'data/11042023_onepot_gRNAdelay_realdeal.csv'
    file4 = 'data/20230420_HPV16_dreamtaq.xls'
    main(file2,file4)