% "Direct detection of SARS-CoV-2 using CRISPR-Cas13a and a mobile phone-based device", 
% Fozouni, Son, Díaz de León Derby et al. Cell 2020
% 
% The software converts the raw RGB .dng images into the greyscale .png
% images and quantifies the intensity from regions of interest within three
% different reaction chambers in the same image (typically two sample and a
% control). The software determines the reaction rate in each chamber by
% quantifying the change in fluorescence intensity over time (ie. slope of
% the signal vs time). By comparing the sample rate with that of the
% negative control, the software determines if the sample rate is
% significantly greater than the control rate for a defined confidence
% interval, which we call a positive result. We use a simple linear
% regression to determine the slope of reaction and its confidence
% interval, which we use to compare reactions.
% (developed with Matlab R2020a)
%
% Input: 
%   The time series of Pixel 4 .dng images of the Cas13a reaction.
%   Typically, the three channels in each images contain sample 1 (top),
%   sample 2 (middle), and a negative control (bottom) (Figure 1). The image
%   file name contains a timestamp with the format YYYYMMDDHHMMSS following
%   '_' (Eg. patient_20200815175841.dng) in order for the software to find
%   the time of acquisition.
%
% Output variables: 
%   roinorm: Signal time-trajectories of sample. 
%       First column = sample 1, Second column = sample 2, Third column = negative control.
% 	fitresult: Slope and binary test result of each sample
%       First raw = sample 1 slope, rmse of fit, confidence interval of slope, binary test result
%       Second raw = sample 2 slope, rmse of fit, confidence interval of slope, binary test result
%       Third raw = Negative control slope, rmse of fit, confidence interval of slope, binary test result
%
% Output plots:
%   Raw signal trajectories of individual samples.
%   Normalized signal trajectories of individual samples. Raw signal is normalized by the signal at time = 0.

close all;
clear all;

% set ROI location and size
ROIx=[830 830 830];
ROIy=[555 770 985];
ROIw=400*ones(1,3);
ROIh=70*ones(1,3);

% phone sensor background level. Empirically determined.
bk=60;

% Ignore the pixel if its submosaic green channel values are more different than this
mismatchThres=0.5;

% fit parameters
tfit1=0;    % start time of fit in minutes
tfit2=tfit1+30; % end time of fit in minutes
conf=0.95;  % confidence interval for the slope estimation

% parameters for outlier rejection
win=5;  % specifying the window size for detecting local outliers
OLthres=1.5;    % detection threshold factor in scaled Median absolute deviation (MAD)

fig=figure(1);
% set(fig, 'position', [50 600 400 300]);
M=0.5;  % image display magnification

[name, place] = uigetfile('./example data/*.dng', 'Select One or More Files', ...
    'MultiSelect', 'on');  

name = sortrows(name);

[c,Nfile]=size(name);
roivals=zeros(Nfile,3); % [num frame, elapsed seconds, ROI va11, ROI val2, ROIval3]

for i=1:Nfile
    
    % Convert .dng (RGB) to .png (Greyscale)
    current_name=cell2mat(name(i));
    img = imread(fullfile(place, current_name));
    fprintf('Processing %s...\n', current_name);
    
    mosaic2=zeros(size(img));
    mosaic3=zeros(size(img));
    [x,y]=size(img);
    mosaic2(2:2:x,1:2:y-1)=1;   % first green pixel in the 2x2 Bayer filter
    mosaic3(1:2:x-1,2:2:y)=1;   % second green pixel in the 2x2 Bayer filter

    ind2=find(mosaic2);
    ind3=find(mosaic3);

    green2=zeros((size(img)/2));
    green3=zeros((size(img)/2));
    green=zeros((size(img)/2));
    
    for j=1:1:((x/2)*(y/2))
        green2(j)=img(ind2(j));
        green3(j)=img(ind3(j));
        green_diff=abs(green2(j)-green3(j))/(green2(j)+green3(j));
        if green_diff>mismatchThres   % ignore the pixel if two green channels are more different than 50%
            green(j)=0;
        else
            if green2(j)>=1023 | green3(j)>=1023    % ignore if either one of the green channels are saturated
                green(j)=0;
            else
                % average the two green channels 
                green(j)=round((green2(j)+green3(j))/2);
            end
        end
    end
    
    green=uint16(green); 

    % get ROI values
    ftime_start=strfind(current_name, '_')+7;
    ftime_end=ftime_start+7;
    t_now=current_name(ftime_start:ftime_end);  
    t_now_time=datetime(t_now, 'InputFormat', 'DDHHmmss');
    if i==1
        t0_time=t_now_time;
        roivals(i, 1:2)=[i, 0];   % elapsed time in seconds
    else
        roivals(i, 1:2)=[i, seconds(t_now_time-t0_time)];   % elapsed time in seconds
    end
    imshow(imresize(green,M),[1, mean2(green)*7]);

    for j=1:length(ROIw)
        rectangle('position', [ROIx(j), ROIy(j), ROIw(j), ROIh(j)]*M, 'edgecolor', 'y',...
            'linewidth', 2);
        text((ROIx(j)-round(ROIw(j)/2))*M, ROIy(j)*M, num2str(j), 'Color', 'y');
        text(1,1,sprintf('Frame %d\n', i), 'color', 'k', 'fontsize', 14);
    end
    pause(0.01);
    
    if i==1
        broi=input('Are ROIs set correctly? (yes=1, no=0)');
        if broi~=1
            disp('Adjust ROIs in the code');
            return;
        end
    end

    for j=1:length(ROIw)    % get the mean ROI values
        I=imcrop(green, [ROIx(j), ROIy(j), ROIw(j), ROIh(j)]);
        roivals(i, j+2)=mean2(nonzeros(I));
    end 
end

fig=figure(2);
% set(fig, 'position', [800 550 400 300]);
hold on;
for j=1:length(ROIw)
    plot(roivals(:,2)/60, roivals(:,j+2), '.-');
end
title('Raw trajectories');
xlabel('Time (min)');
ylabel('Raw signal');
legend('Sample1', 'Sample2', 'Negative');

fig=figure(3);
% set(fig, 'position', [800 200 400 300]);
hold on;
roinorm=zeros(length(roivals(:,1)), 3);

p=zeros(length(ROIw), 2);
fitresult=zeros(length(ROIw), 4);   
for j=1:length(ROIw)
    roinorm(:,j)=(roivals(:,j+2)-bk)/((roivals(1,j+2))-bk);
    % exclude outliers
    [B,TF] = rmoutliers(roinorm(:,j),'movmedian', win, 'ThresholdFactor', OLthres);
    t=roivals(~TF,2);
    p(j,:)=plot(roivals(:,2)/60, roinorm(:,j),'.', t/60, B, '-');
    % linear fit
    tidx=find(t/60>tfit1 & t/60<tfit2);
    [f, gof]=fit(t(tidx)/60, B(tidx), 'poly1');
    fitresult(j,1)=f.p1;
    fitresult(j,2)=gof.rmse;
    plot(t(tidx)/60, polyval([f.p1 f.p2], t(tidx)/60), '-r');
    ci=confint(f, conf);    
    fitresult(j,3)=f.p1-ci(1,1);  % obtain the confidence interval of slope
end
title('Normalized trajectories');
xlabel('Time (min)');
ylabel('Normalized signal');
legend([p(1,1), p(2,1), p(3,1)], {'Sample1', 'Sample2', 'Negative'});

% sample is positive if its slope does not overlap with that of the
% control within the designated confidence interval
if fitresult(1,1)-fitresult(1,3)>fitresult(3,1)+fitresult(3,3)
    fprintf('Sample 1: positive\n');
    fitresult(1,4)=1;
else
    fprintf('Sample 1: negative\n');
    fitresult(1,4)=0;
end
if fitresult(2,1)-fitresult(2,3)>fitresult(3,1)+fitresult(3,3)
    fprintf('Sample 2: positive\n');
    fitresult(2,4)=1;
else
    fprintf('Sample 2: negative\n');
    fitresult(2,4)=0;
end

