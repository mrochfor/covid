### import shit ###




### retrieve data from csvs (plate reader and qPCR) ###



########### PLATE READER STUFF ###########




### arrange data nicely in dataframe - dataframe for sample and dataframe for replicates, time vector (in minutes and seconds) *PLATEREADER ###


DONE


### dataframe as above but seperate set were the fluoresnces have been "baseline corrected" or "zero corrected" - they all start at zero ###


Just subtract the value at time zero from each value in the entire column
DONE

### calculation of "endpoint" slope - for each sample and each replicate set - stored in dataframe *able to be changed to various/multiple endpoint times ###


calculate this between first data point and each subsequent data point and create new column like B1_ENDPOINT_SLOPE
DONE


### calculation of standard deviation each replicate set - stored in dataframe *able to be changed to various/multiple endpoint times ###


send jake back a csv to label stuff - replicate set, concentration, label


### calculation of mean (per time point) of each replicate set - stored in dataframe *able to be changed to various/multiple endpoint times ###





### calculation of "point" slope (each slope over delta T between readings) - for each sample and each replicate set - stored in dataframe *able to be changed to end at various/multiple times ###


slope between point and the pont before it
for replicate point slope try to average the 3 point slopes and also try the point slope and the means calculated in the previous step


### calculation of confidence interval that doesnt intersect with X confidence interval of negative - for replicates (for endpoint slope, point slope and point slope with alg) ###


use the replicate set of negatives vs replicate set of some concentration


### link concentration/ add concentration vector to dataframe - to graph (for example) concentration vs. fluorescence (at some time t) ###





########### ALGORITHM STUFF - FOR IMPROVED DECISION ###########





### check every sample (and point in sample spanning time vector) against "slope (point slope) magnitude" - collect in scoring vector (for s,m in zip(sample,magnitude_vector) - is s > m? yes give 1 - no give 0) - do ###

do this at the sample level

B1_MAGNITUDE_SCORING_VECTOR = Do for every well at every possible value of m like from min of all point slopes to the max of like either the low or negative point slope set



### check every sample (and point in sample spanning time vector) against "slope (point slope) consecutive" - collect in scoring vector (for s,c in zip(sample,consecutive_vector) - is s > c? yes give 1 - no give 0) - do ###

do this at the sample level

this is going to be driven by the B1_MAGNITUDE_SCORING_VECTOR calculated in the previous set 



### merge magnitude_scoring_vector and consecutive_scoring_vector to form matrix (outer product) - to be graphed to invision where thresholds could be placed ###


make a matrix for each replicate set and calculate the percentage of samples that meet the criteria for that cell
x axis = magnitude cutoffs
y axis = consecutive cutoffs



### scores given to each sample for how many consecutive point slopes were over some magnitude_instance - scores collected per sample and per replicate, we can use this to graph later and also to then to classification ###
#ill refer to this as algorithm score later




########### qPCR STUFF ###########




### arrange data nicely in dataframe - dataframe for sample and dataframe for replicates, time vector (in cycles) ###




### calculate CT value of each sample -  ###
#to calculate CT value
# 1. normalize all samples to zero fluorescence and subtracted to zero
# 2. calculate a threshold X% above threshold (usually 10-25%) use 20%
# 3. find out at which cycle the sample crosses this threshold




### calculate the average CT per replicate - place in dataframe ###




### calculate the standard deviation of CT per replicate - place in dataframe ###





################################################################################################## GRAPHING STUFF #################################################################################################################




### labeling the replicates ###
### changing colors - my prof is suuuuper colorblind so def need to change shit here ###





########### PLATE READER STUFF ###########





### 1. point plot - Fluorescence vs time - with error bars being controlable confidence interval (i sent you this already) - option to take every X data point from set (sometimes may only wanna graph like every third for example) ###





### 2. point plot - endpoint slope vs end time - controllable: confidence interval (on error bars), end time ###





### 3. point plot - endpoint slope vs concentration (at endtime X) - controllable: confidence interval (on error bars), end time ###


Same as 2?


### 4. point plot - endpoint slope vs concentration vs time (either 3D point or a vector of 2D plots OR both) - controllable: confidence interval (on error bars), end time ###


Same as 2?


### 5. point plot - point slope vs time - controllable: confidence interval (on error bars), end time ###





### 6. point plot - point slope vs time vs concentration (3D) - controllable: confidence interval (on error bars), end time ###






### 7. point plot - confidence (that the positive escapes from negative) vs time - controllable: confidence of negative to be escaped from, end time ###






### 8. point plot - confidence (that the positive escapes from negative) vs time (more continous) - controllable: confidence of negative to be escaped from, end time ###






### 9. point plot - **with alg in place** confidence (that the positive escapes from negative) vs time - controllable: confidence of negative to be escaped from, end time ###





### 10. point plot - **with alg in place** A heat map kinda like the one in figure 4d of this paper but not so fuckin dogshit: https://www.science.org/doi/10.1126/science.aar6245 ###
#illustrate the difference between with and without the alg
#also later will add PCR to this so we see how everything agrees (or not)





 
### 11. Heat map (matrix with colors per each square - discrete intersection between consecutive_score_vector and magnitude_score_vector) - find the points with highest score, then find the centroid of this segment (most safe place to make correct call) and indicate these cuttoffs ###







### 12. 3D alg graph - x = magnitude vector (vector of all cutoffs investigated over), y = consecutive vector (vector of all cutoffs investigated over), z = scoring vector (the resulting %of positive samples correctly called) -  forming a mountain in center and highest point(s) are where best cuttoffs are - find with gradient ###







### 13. Algorithm score vs. time (at set magnitude and consecutive cuttoffs) ###








### 14. Algorithm score vs. time - with several classifications (high, med, low, negative) ###







### 15. Algorithm score vs. time - binary classification ###






### 16. Signal to noise - define the negative as equal to 1 and the then graph: x-axis = time and y-axis = times/folds more fluorescent than the negative (signal/noise) OR (positive/negative) ###






### 17-33. 1-16 repeated but with noise filtration  ###





### weird dream graph - a set of concentric spheres indicating various standard deviations of the negtive, once you escape each sphere you are labeled positive with some degree of confidence ###
#potential axis in R3 = time, flourescence, ct, concentration
#maybe R2 with axis = confidence and time and *******



########### qPCR STUFF ###########





### like number 10. but just now including a column for CTs ###





### CT vs. fluorescence (at some endtime) - controllable: confidence interval (on error bars), end time - like figure 4e: https://www.sciencedirect.com/science/article/pii/S0956566323001215  ###





### CT vs. fluorescence slope (at some endtime) - controllable: confidence interval (on error bars), end time  ###





### CT vs. Algorithm score (not binary classified) - controllable: end time  ###





### CT vs. Algorithm decision (after binary classification) - controllable: end time  ###






### CT vs. Algorithm classification (high, med, low -  we can correspond this to CT values usually something like: CT<25 = high,  25<CT<32 = medium, CT>32 = low) - controllable: end time  ###






### Prediction of CT value: Confidence in predition (to connect fluorescence slope to CT) vs time (as we discussed for that previous system and maybe can also use on other system! But we may not have enough data in this set for this) ###






##### Paper Plan #####
# Figure 1. = several subplots of plate reader data
# Figure 2. = several subplots with noise filters thrown on
# Figure 3. = several subplots of plate reader data vs PCR data
# Figure 4. = several subplots of plate reader with algorith (with and without PCR stuff)
# Figure 5. = several figures visualizing what and where the algorithm is acting (cuttoff thresholds, optimium settings, etc) *maybe swap this order with figure 3
# Figure 6. = creative data visualizations like the one i labeled werid dream graph (could also just include these in the previous 4 figures... 4 is enough)

# Figure 0. =  layout schematic - showing whole process from data collection to output (where it may be used (hospital), etc)