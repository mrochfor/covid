import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sns
from scipy.optimize import curve_fit
import scipy
from functools import reduce
import datetime
import time
import string
import matplotlib.lines as mlines

####################################################################################################################### CHANGE HERE #######################################################################################################################
### import file ###
directory = 'Desktop/Biochemistry/biochemtofetch/02052023/'
filename = '02052023_cas14LODsweep.csv'
# graphoutput = 'C:/Users/jlesinski/Desktop/Python/pythongraphs'
graphoutput = 'C:/Users/jlesinski/Desktop/Biochemistry/biochemgraphs/'
outputname = '02052023_cas14LODsweep.png'
df = pd.read_csv(r'C:/Users/jlesinski/'+directory+'/'+filename,skiprows=91)
listuse = pd.Series.tolist(df)



rep_x_1_index_start = 0
rep_x_1_index_finish = 100
### create time vector ###
time_col = df.loc[:,"Time"]
time_col_list = pd.Series.tolist(time_col)
time_col_list = time_col_list[0:rep_x_1_index_finish+1]
time_col_list_use =[]
for i in time_col_list:
    if ':' in i:
        time_col_list_use.append(i)
    else:
        break
ftr = [3600,60,1]
time_list = []
for t in time_col_list_use:
    time_list.append(sum([a*b for a,b in zip(ftr, map(int,t.split(':')))]))


time_list_mins = []
for t in time_list:
    if t == 0:
        time_list_mins.append(t)
    else:
        time_list_mins.append(int(t/60))

deltaT_perread = time_list_mins[1]- time_list_mins[0]
time_list = time_list_mins

# ###### Time Control #######
# closeto = min(time, key=lambda x:abs(x-2100))
# time = list(time)
# locationofcloseto = time.index(closeto)
# time = time[0:locationofcloseto]


### create 384 plate matrix ###
listuse_plate = []
for i in listuse:
    listuse_plate.append(i[2:])
j = np.arange(1,25,1)
x = []
for i in j:
    x.append(str(i))
y = list(string.ascii_uppercase)
y = y[:16]
xy = []
for j in y:
    for i in x:
        xy.append(j+i)
length = int(len(xy)) # Amount of lists you want to create
emptylist = []
for i,j in zip(range(1, length+1),xy):
    command = "" # This line is here to clear out the previous command
    command = 'emptylist.append('+str({str(j):[]})+')'
    exec(command)



### create time based vector for each well of plate ###
for d in listuse_plate:
    for data,well,dictname in zip(d,emptylist,xy):
        well[dictname].append(data)



### Dilution ###
# In pico molar #
# concentration = 17.7#[ng/ul]
# starting_dilution = (3/50)
# denom_of_dilution = 3
# number_of_dil = 8 #including initial
# dilution_vector = [starting_dilution]
# while len(dilution_vector) < number_of_dil:
#   dilution_vector.append(dilution_vector[-1]*(1/denom_of_dilution))
# dilution_vector = np.array(dilution_vector)
# dilution_vector = dilution_vector*concentration

# avg_strand_length = 1000
# illum_multi = 660 #g/mol
# conc_nM = []
# for conc_ngperul in dilution_vector:
#   conc_nM.append((conc_ngperul/(illum_multi*avg_strand_length))*(10**6))
# dilution_vector = conc_nM
# ### With picomolar concentraiton ###
# dilution_vector = np.array(dilution_vector)
# dilution_vector = dilution_vector*1000
# dilution_vector_pico = []
# for i in dilution_vector:
#   dilution_vector_pico.append(np.round(i,decimals = 5))
# print(dilution_vector_pico)


####################################################################################################################### CHANGE HERE #######################################################################################################################
### Merge replicates ###

# rep_1 = ['B2']
# rep_2 = ['C2']
# rep_3 = ['D2']
# rep_4 = ['E2']


rep_1 = ['K14','K15'] 
rep_2 = ['L14','L15'] 
rep_3 = ['M14','M15'] 
rep_4 = ['N14','N15'] 
rep_5 = ['O14','O15']
rep_6 = ['P14','P15'] 

# rep_7 = ['M11'] 
# rep_8 = ['M12'] 

# rep_9 = ['M13'] # complexed 25nM cas, 200pM - negative
# rep_10 = ['J14','J15'] # Not complexed 25nM cas, 200pM - negative
# rep_11 = ['J6','J7','J8']
# rep_12 = ['K6','K7','K8']
# rep_13 = ['L6','L7','L8']
# rep_14 = ['M6','M7','M8'] #new one pot off lowest concentration 0.2 cp/ul

# rep_17 = ['B2','B3','B4'] #new one pot off negative
# rep_18 = ['C2','C3','C4'] #new one pot off target DNA
# rep_15 = ['D2','D3','D4'] #old one pot negative
# rep_16 = ['E2','E3','E4'] #old one pot off target DNA


# rep_15 = ['P21','P22','P23']
# rep_16 = ['O21','O22','O23']
# rep_17 = ['N21','N22','N23']
# rep_18 = ['M21','M22','M23']

####################################################################################################################### CHANGE HERE #######################################################################################################################
# rep_list = [rep_1, rep_2,rep_3, rep_4]
# rep_list = [rep_1, rep_2,rep_3, rep_4,rep_5, rep_6,rep_7, rep_8,rep_9, rep_10,rep_11,rep_12, rep_13]
# rep_list = [rep_1, rep_2,rep_3, rep_4,rep_5, rep_6,rep_7, rep_8,rep_9, rep_10,rep_11,rep_12, rep_13, rep_14, rep_15,rep_16, rep_17, rep_18]
# rep_list = [rep_1, rep_2,rep_3, rep_4,rep_5, rep_6,rep_7, rep_8,rep_9]
rep_list = [rep_1, rep_2,rep_3, rep_4,rep_5, rep_6]
# rep_list = [rep_1]

for e,number in zip(range(len(rep_list)),enumerate(rep_list)):
    command = "" # This line is here to clear out the previous command
    command = 'replicate_'+str(number[0]+1)+ '=[]'
    exec(command)

####################################################################################################################### CHANGE HERE #######################################################################################################################
# replicate_list = [replicate_1, replicate_2, replicate_3, replicate_4]
# replicate_list = [replicate_1, replicate_2,replicate_3, replicate_4,replicate_5,replicate_6, replicate_7,replicate_8, replicate_9,replicate_10,replicate_11, replicate_12,replicate_13, replicate_14,replicate_15, replicate_16,replicate_17,replicate_18]
# replicate_list = [replicate_1, replicate_2,replicate_3, replicate_4,replicate_5,replicate_6, replicate_7,replicate_8, replicate_9]
replicate_list = [replicate_1, replicate_2,replicate_3, replicate_4,replicate_5,replicate_6]
# replicate_list = [replicate_1]

list_of_lists = [time_list*len(rep_1)]
time = []
for sublist in list_of_lists:
    for item in sublist:
        time.append(item)


for rep,replicate in zip(rep_list,replicate_list):
    list_of_lists = []
    for r in rep:
        list_of_lists.append(emptylist[xy.index(r)][r][:len(time_list)])
    for sublist in list_of_lists:
        for item in sublist:
            if item == 'OVRFLW':
                replicate.append(float(100000))
            else:
                replicate.append(float(item))

# for e,number in zip(range(len(rep_list)),enumerate(rep_list)):
#   command = "" # This line is here to clear out the previous command
#   command = 'replicate_'+str(number[0]+1)+ '=[]'
#   exec(command)

replicate_list_name = []
for i,num in zip(range(len(rep_list)),enumerate(rep_list)):
    replicate_list_name.append('rep'+str(num[0]+1))

for rep_name,replicate,number in zip(replicate_list_name,replicate_list,enumerate(replicate_list)):
    rep_temp = {rep_name:replicate,'time':time}
    command = "" # This line is here to clear out the previous command
    command = 'df_rep_'+str(number[0]+1)+'=pd.DataFrame(data=rep_temp)'
    exec(command)
#########################################################################################################################
# print("before zeroing: ")
# for i in range(4):
#      print(df_rep_1['rep1'].iat[i])
     


####################################################################################################################### CHANGE HERE #######################################################################################################################
rep_x_2_index_start = rep_x_1_index_finish+1
rep_x_2_index_finish = rep_x_2_index_start+rep_x_1_index_finish
# rep_x_3_index_start = rep_x_2_index_finish+1
# rep_x_3_index_finish = rep_x_3_index_start+rep_x_1_index_finish



####################################################################################################################### CHANGE HERE #######################################################################################################################
#starting_point
df_rep_1_sp = df_rep_1['rep1'].iat[0]
for i in range(rep_x_1_index_start,rep_x_1_index_finish):
    df_rep_1['rep1'].iat[i] = df_rep_1['rep1'].iat[i]-df_rep_1_sp


df_rep_1_sp = df_rep_1['rep1'].iat[rep_x_2_index_start]

for i in range(rep_x_2_index_start,rep_x_2_index_finish):
    df_rep_1['rep1'].iat[i] = df_rep_1['rep1'].iat[i]-df_rep_1_sp
    
# df_rep_1_sp = df_rep_1['rep1'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_1['rep1'].iat[i] = df_rep_1['rep1'].iat[i]-df_rep_1_sp


##################rep2###################################

df_rep_2_sp = df_rep_2['rep2'].iat[0]
for i in range(rep_x_1_index_start,rep_x_1_index_finish):
    df_rep_2['rep2'].iat[i] = df_rep_2['rep2'].iat[i]-df_rep_2_sp


df_rep_2_sp = df_rep_2['rep2'].iat[rep_x_2_index_start]

for i in range(rep_x_2_index_start,rep_x_2_index_finish):
    df_rep_2['rep2'].iat[i] = df_rep_2['rep2'].iat[i]-df_rep_2_sp
    

# df_rep_2_sp = df_rep_2['rep2'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_2['rep2'].iat[i] = df_rep_2['rep2'].iat[i]-df_rep_2_sp


##################rep3###################################

df_rep_3_sp = df_rep_3['rep3'].iat[0]
for i in range(rep_x_1_index_start,rep_x_1_index_finish):
    df_rep_3['rep3'].iat[i] = df_rep_3['rep3'].iat[i]-df_rep_3_sp


df_rep_3_sp = df_rep_3['rep3'].iat[rep_x_2_index_start]

for i in range(rep_x_2_index_start,rep_x_2_index_finish):
    df_rep_3['rep3'].iat[i] = df_rep_3['rep3'].iat[i]-df_rep_3_sp
    
# df_rep_3_sp = df_rep_3['rep3'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_3['rep3'].iat[i] = df_rep_3['rep3'].iat[i]-df_rep_3_sp


##################rep4###################################

df_rep_4_sp = df_rep_4['rep4'].iat[0]
for i in range(rep_x_1_index_start,rep_x_1_index_finish):
    df_rep_4['rep4'].iat[i] = df_rep_4['rep4'].iat[i]-df_rep_4_sp


df_rep_4_sp = df_rep_4['rep4'].iat[rep_x_2_index_start]

for i in range(rep_x_2_index_start,rep_x_2_index_finish):
    df_rep_4['rep4'].iat[i] = df_rep_4['rep4'].iat[i]-df_rep_4_sp
    
# df_rep_4_sp = df_rep_4['rep4'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_4['rep4'].iat[i] = df_rep_4['rep4'].iat[i]-df_rep_4_sp

##################rep5###################################

df_rep_5_sp = df_rep_5['rep5'].iat[0]
for i in range(rep_x_1_index_start,rep_x_1_index_finish):
    df_rep_5['rep5'].iat[i] = df_rep_5['rep5'].iat[i]-df_rep_5_sp


df_rep_5_sp = df_rep_5['rep5'].iat[rep_x_2_index_start]

for i in range(rep_x_2_index_start,rep_x_2_index_finish):
    df_rep_5['rep5'].iat[i] = df_rep_5['rep5'].iat[i]-df_rep_5_sp
    
# df_rep_5_sp = df_rep_5['rep5'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_5['rep5'].iat[i] = df_rep_5['rep5'].iat[i]-df_rep_5_sp

##################rep6###################################

df_rep_6_sp = df_rep_6['rep6'].iat[0]
for i in range(rep_x_1_index_start,rep_x_1_index_finish):
    df_rep_6['rep6'].iat[i] = df_rep_6['rep6'].iat[i]-df_rep_6_sp


df_rep_6_sp = df_rep_6['rep6'].iat[rep_x_2_index_start]

for i in range(rep_x_2_index_start,rep_x_2_index_finish):
    df_rep_6['rep6'].iat[i] = df_rep_6['rep6'].iat[i]-df_rep_6_sp
    
# df_rep_6_sp = df_rep_6['rep6'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_6['rep6'].iat[i] = df_rep_6['rep6'].iat[i]-df_rep_6_sp

##################rep7###################################

# df_rep_7_sp = df_rep_7['rep7'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_7['rep7'].iat[i] = df_rep_7['rep7'].iat[i]-df_rep_7_sp


# df_rep_7_sp = df_rep_7['rep7'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_7['rep7'].iat[i] = df_rep_7['rep7'].iat[i]-df_rep_7_sp
    
# df_rep_7_sp = df_rep_7['rep7'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_7['rep7'].iat[i] = df_rep_7['rep7'].iat[i]-df_rep_7_sp

##################rep8###################################

# df_rep_8_sp = df_rep_8['rep8'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_8['rep8'].iat[i] = df_rep_8['rep8'].iat[i]-df_rep_8_sp


# df_rep_8_sp = df_rep_8['rep8'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_8['rep8'].iat[i] = df_rep_8['rep8'].iat[i]-df_rep_8_sp
    
# df_rep_8_sp = df_rep_8['rep8'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_8['rep8'].iat[i] = df_rep_8['rep8'].iat[i]-df_rep_8_sp

##################rep9###################################

# df_rep_9_sp = df_rep_9['rep9'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_9['rep9'].iat[i] = df_rep_9['rep9'].iat[i]-df_rep_9_sp


# df_rep_9_sp = df_rep_9['rep9'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_9['rep9'].iat[i] = df_rep_9['rep9'].iat[i]-df_rep_9_sp
    
# df_rep_9_sp = df_rep_9['rep9'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_9['rep9'].iat[i] = df_rep_9['rep9'].iat[i]-df_rep_9_sp

##################rep10###################################

# df_rep_10_sp = df_rep_10['rep10'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_10['rep10'].iat[i] = df_rep_10['rep10'].iat[i]-df_rep_10_sp


# df_rep_10_sp = df_rep_10['rep10'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_10['rep10'].iat[i] = df_rep_10['rep10'].iat[i]-df_rep_10_sp
    
# df_rep_10_sp = df_rep_10['rep10'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_10['rep10'].iat[i] = df_rep_10['rep10'].iat[i]-df_rep_10_sp

##################rep11###################################

# df_rep_11_sp = df_rep_11['rep11'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_11['rep11'].iat[i] = df_rep_11['rep11'].iat[i]-df_rep_11_sp


# df_rep_11_sp = df_rep_11['rep11'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_11['rep11'].iat[i] = df_rep_11['rep11'].iat[i]-df_rep_11_sp
    
# df_rep_11_sp = df_rep_11['rep11'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_11['rep11'].iat[i] = df_rep_11['rep11'].iat[i]-df_rep_11_sp

# ##################rep12###################################

# df_rep_12_sp = df_rep_12['rep12'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_12['rep12'].iat[i] = df_rep_12['rep12'].iat[i]-df_rep_12_sp


# df_rep_12_sp = df_rep_12['rep12'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_12['rep12'].iat[i] = df_rep_12['rep12'].iat[i]-df_rep_12_sp
    
# df_rep_12_sp = df_rep_12['rep12'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_12['rep12'].iat[i] = df_rep_12['rep12'].iat[i]-df_rep_12_sp

# ##################rep13###################################

# df_rep_13_sp = df_rep_13['rep13'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_13['rep13'].iat[i] = df_rep_13['rep13'].iat[i]-df_rep_13_sp


# df_rep_13_sp = df_rep_13['rep13'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_13['rep13'].iat[i] = df_rep_13['rep13'].iat[i]-df_rep_13_sp
    
# df_rep_13_sp = df_rep_13['rep13'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_13['rep13'].iat[i] = df_rep_13['rep13'].iat[i]-df_rep_13_sp

# ##################rep14###################################

# df_rep_14_sp = df_rep_14['rep14'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_14['rep14'].iat[i] = df_rep_14['rep14'].iat[i]-df_rep_14_sp


# df_rep_14_sp = df_rep_14['rep14'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_14['rep14'].iat[i] = df_rep_14['rep14'].iat[i]-df_rep_14_sp
    
# df_rep_14_sp = df_rep_14['rep14'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_14['rep14'].iat[i] = df_rep_14['rep14'].iat[i]-df_rep_14_sp

# ##################rep15###################################

# df_rep_15_sp = df_rep_15['rep15'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_15['rep15'].iat[i] = df_rep_15['rep15'].iat[i]-df_rep_15_sp


# df_rep_15_sp = df_rep_15['rep15'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_15['rep15'].iat[i] = df_rep_15['rep15'].iat[i]-df_rep_15_sp
    
# df_rep_15_sp = df_rep_15['rep15'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_15['rep15'].iat[i] = df_rep_15['rep15'].iat[i]-df_rep_15_sp

# ##################rep16###################################

# df_rep_16_sp = df_rep_16['rep16'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_16['rep16'].iat[i] = df_rep_16['rep16'].iat[i]-df_rep_16_sp


# df_rep_16_sp = df_rep_16['rep16'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_16['rep16'].iat[i] = df_rep_16['rep16'].iat[i]-df_rep_16_sp
    
# df_rep_16_sp = df_rep_16['rep16'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_16['rep16'].iat[i] = df_rep_16['rep16'].iat[i]-df_rep_16_sp

# ##################rep17###################################

# df_rep_17_sp = df_rep_17['rep17'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_17['rep17'].iat[i] = df_rep_17['rep17'].iat[i]-df_rep_17_sp


# df_rep_17_sp = df_rep_17['rep17'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_17['rep17'].iat[i] = df_rep_17['rep17'].iat[i]-df_rep_17_sp
    
# df_rep_17_sp = df_rep_17['rep17'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_17['rep17'].iat[i] = df_rep_17['rep17'].iat[i]-df_rep_17_sp

# ##################rep18###################################

# df_rep_18_sp = df_rep_18['rep18'].iat[0]
# for i in range(rep_x_1_index_start,rep_x_1_index_finish):
#     df_rep_18['rep18'].iat[i] = df_rep_18['rep18'].iat[i]-df_rep_18_sp


# df_rep_18_sp = df_rep_18['rep18'].iat[rep_x_2_index_start]

# for i in range(rep_x_2_index_start,rep_x_2_index_finish):
#     df_rep_18['rep18'].iat[i] = df_rep_18['rep18'].iat[i]-df_rep_18_sp
    
# df_rep_18_sp = df_rep_18['rep18'].iat[rep_x_3_index_start]

# for i in range(rep_x_3_index_start,rep_x_3_index_finish):
#     df_rep_18['rep18'].iat[i] = df_rep_18['rep18'].iat[i]-df_rep_18_sp



####################################################################################################################### CHANGE HERE #######################################################################################################################
# fig = plt.figure(figsize=(3,2.5))
# ax1 = fig.add_subplot(111)

confidene_interval = 68

color_vector = sns.color_palette(palette='colorblind', n_colors=9, desat=None, as_cmap=False)
#color_vector = sns.cubehelix_palette(n_colors=9, start=0.5, rot=0.75, as_cmap=False)

fig = plt.figure()
ax1 = fig.add_subplot(111)
# ax2 = fig.add_subplot(122)

sns.axes_style({'font.family': ['arial']})
sns.pointplot(data=df_rep_1,
              x='time',
              y='rep1',
              scale=0.2,
              ax=ax1,
              join=False,
              errorbar=('ci', confidene_interval),
              # hue='red',
              # style='crRNA',
              #ci='sd',          
              err_style='bars',
              errwidth=0.2,
              color= color_vector[0])

sns.pointplot(data=df_rep_2,
              x='time',
              y='rep2',
              scale=0.2,
              ax=ax1,
              join=False,
              errorbar=('ci', confidene_interval),
              # hue='red',
              # style='crRNA',
              #ci='sd',          
              err_style='bars',
              errwidth=0.2,
              color= color_vector[1])

sns.pointplot(data=df_rep_3,
              x='time',
              y='rep3',
              scale=0.2,
              ax=ax1,
              join=False,
              errorbar=('ci', confidene_interval),
              # hue='red',
              # style='crRNA',
              #ci='sd',          
              err_style='bars',
              errwidth=0.2,
              color= color_vector[2])

sns.pointplot(data=df_rep_4,
              x='time',
              y='rep4',
              scale=0.2,
              ax=ax1,
              join=False,
              errorbar=('ci', confidene_interval),
              # hue='red',
              # style='crRNA',
              #ci='sd',          
              err_style='bars',
              errwidth=0.2,
              color= color_vector[3])

sns.pointplot(data=df_rep_5,
              x='time',
              y='rep5',
              scale=0.2,
              ax=ax1,
              join=False,
              errorbar=('ci', confidene_interval),
              # hue='red',
              # style='crRNA',
              #ci='sd',          
              err_style='bars',
              errwidth=0.2,
              color= color_vector[4])

sns.pointplot(data=df_rep_6,
              x='time',
              y='rep6',
              scale=0.2,
              ax=ax1,
              join=False,
              errorbar=('ci', confidene_interval),
              # hue='red',
              # style='crRNA',
              #ci='sd',          
              err_style='bars',
              errwidth=0.2,
              color= color_vector[5])

# sns.pointplot(data=df_rep_7,
#               x='time',
#               y='rep7',
#               scale=0.2,
#               ax=ax1,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[6])

# sns.pointplot(data=df_rep_8,
#               x='time',
#               y='rep8',
#               scale=0.2,
#               ax=ax1,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[7])

# sns.pointplot(data=df_rep_9,
#               x='time',
#               y='rep9',
#               scale=0.2,
#               ax=ax1,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[8])

# sns.pointplot(data=df_rep_10,
#               x='time',
#               y='rep10',
#               scale=0.2,
#               ax=ax1,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[8])

dot_1 = mlines.Line2D([], [], color = color_vector[0], marker='o',ls='', markersize=2, label= '4.54 nM Target DNA in rxn')
dot_2 = mlines.Line2D([], [], color = color_vector[1],marker='o',ls='', markersize=2, label= '454 pM Target DNA in rxn')
dot_3 = mlines.Line2D([], [], color = color_vector[2],marker='o',ls='', markersize=2, label= '45.4 pM Target DNA in rxn')

dot_4 = mlines.Line2D([], [], color = color_vector[3],marker='o',ls='', markersize=2, label= '4.54 pM Target DNA in rxn')
dot_5 = mlines.Line2D([], [], color = color_vector[4],marker='o',ls='', markersize=2, label= '454 fM Target DNA in rxn')
dot_6 = mlines.Line2D([], [], color = color_vector[5],marker='o',ls='', markersize=2, label= 'Negative')

# dot_7 = mlines.Line2D([], [], color = color_vector[6],marker='o',ls='', markersize=2, label= 'High [Cas], Low [gRNA]')
# dot_8 = mlines.Line2D([], [], color = color_vector[7],marker='o',ls='', markersize=2, label= 'Med [Cas], Low [gRNA]')
# dot_9 = mlines.Line2D([], [], color = color_vector[8],marker='o',ls='', markersize=2, label= 'Low [Cas], Low [gRNA]')
# dot_10 = mlines.Line2D([], [], color = color_vector[8],marker='o',ls='', markersize=2, label= 'Negative, Not Complexed - High [Cas]')


plt.legend(handles=[dot_1, dot_2, dot_3, dot_4, dot_5, dot_6], frameon= False, handletextpad=0.1,fontsize=6)
# plt.legend(handles=[dot_4, dot_8, dot_9, dot_10], frameon= False, handletextpad=0.1,fontsize=6)
# calcultating the maximum flourescence for the y axis 
reduced_dataframe = df.iloc[:len(time_list),2:].fillna(0)
# max_yaxis = np.max(np.array(reduced_dataframe, dtype=int).flatten())

x_limitor = 100

ax1.set_xlabel('Time [Minutes]',fontsize = 6.5)
ax1.set_ylabel('Fluorescence [AFU]',fontsize = 6.5)
ax1.set_xticks((np.arange(0,x_limitor,x_limitor/15)))
# ax1.set_yticks((np.arange(0,40000,40000/8)))

ax1.yaxis.set_tick_params(labelsize = 6)
ax1.xaxis.set_tick_params(labelsize = 6)

ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)
ax1.spines['bottom'].set_visible(True)
ax1.spines['left'].set_visible(True)

color_name = "black"
# ax2.spines["top"].set_color(color_name)
ax1.spines["bottom"].set_color(color_name)
ax1.spines["left"].set_color(color_name)
# ax2.spines["right"].set_color(color_name)

# ax1.legend(fontsize = 5)
ax1.set_xlim([0, x_limitor- deltaT_perread])
# ax1.set_ylim([-1000, 2000])
ax1.set_title('Cas14 LOD Investigation', fontsize = 6.5)



# sns.pointplot(data=df_rep_8,
#               x='time',
#               y='rep8',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[0])

# sns.pointplot(data=df_rep_9,
#               x='time',
#               y='rep9',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[1])

# sns.pointplot(data=df_rep_10,
#               x='time',
#               y='rep10',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[2])

# sns.pointplot(data=df_rep_11,
#               x='time',
#               y='rep11',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[3])

# sns.pointplot(data=df_rep_12,
#               x='time',
#               y='rep12',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[4])

# sns.pointplot(data=df_rep_13,
#               x='time',
#               y='rep13',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[5])

# sns.pointplot(data=df_rep_14,
#               x='time',
#               y='rep14',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[6])

# sns.pointplot(data=df_rep_17,
#               x='time',
#               y='rep17',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[7])

# sns.pointplot(data=df_rep_18,
#               x='time',
#               y='rep18',
#               scale=0.2,
#               ax=ax2,
#               join=False,
#               errorbar=('ci', confidene_interval),
#               # hue='red',
#               # style='crRNA',
#               #ci='sd',          
#               err_style='bars',
#               errwidth=0.2,
#               color= color_vector[8])

# dot_1 = mlines.Line2D([], [], color = color_vector[0], marker='o',ls='', markersize=2, label= '200,000 cp/uL')
# dot_2 = mlines.Line2D([], [], color = color_vector[1],marker='o',ls='', markersize=2, label= '20,000 cp/uL')
# dot_3 = mlines.Line2D([], [], color = color_vector[2],marker='o',ls='', markersize=2, label= '2,000 cp/uL')

# dot_4 = mlines.Line2D([], [], color = color_vector[3],marker='o',ls='', markersize=2, label= '200 cp/uL')
# dot_5 = mlines.Line2D([], [], color = color_vector[4],marker='o',ls='', markersize=2, label= '20 cp/uL')
# dot_6 = mlines.Line2D([], [], color = color_vector[5],marker='o',ls='', markersize=2, label= '2 cp/uL')

# dot_7 = mlines.Line2D([], [], color = color_vector[6],marker='o',ls='', markersize=2, label= '0.2 cp/uL')
# dot_8 = mlines.Line2D([], [], color = color_vector[7],marker='o',ls='', markersize=2, label= 'Negative')
# dot_9 = mlines.Line2D([], [], color = color_vector[8],marker='o',ls='', markersize=2, label= 'Negative - NSDC')


# plt.legend(handles=[dot_1, dot_2, dot_3, dot_4, dot_5, dot_6, dot_7, dot_8, dot_9], frameon= False, handletextpad=0.1,fontsize=6)
# # calcultating the maximum flourescence for the y axis 
# reduced_dataframe = df.iloc[:len(time_list),2:].fillna(0)
# # max_yaxis = np.max(np.array(reduced_dataframe, dtype=int).flatten())


# ax2.set_xlabel('Time [s]',fontsize = 6.5)
# ax2.set_ylabel('Fluorescence [AFU]',fontsize = 6.5)
# ax2.set_xticks((np.arange(0,x_limitor,x_limitor/4)))
# ax2.set_yticks((np.arange(0,120000,120000/6)))

# ax2.yaxis.set_tick_params(labelsize = 6)
# ax2.xaxis.set_tick_params(labelsize = 6)

# ax2.spines['top'].set_visible(False)
# ax2.spines['right'].set_visible(False)
# ax2.spines['bottom'].set_visible(True)
# ax2.spines['left'].set_visible(True)

# # ax1.legend(fontsize = 5)
# ax2.set_xlim([0, x_limitor])
# ax2.set_ylim([-1000, 100000])
# ax2.set_title('gRNA Delay One-Pot (n=3)', fontsize = 6.5)

fig.subplots_adjust(wspace= 0.5, hspace= 0.15)

fig.savefig(graphoutput+outputname, dpi = 1000, transparent = False)
