# Main script for processing iamge data
# Author: Matt Rochford (mrochfor@ucsc.edu)

# Import packages
import pandas as pd
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
import cv2
#import queue
import time

# Define paths to data for global use
path_NEB = "../data/NEB21_raise/"
path = "../data/"


def main():
    """
    Main function for loading and processing data

    
    Inputs:
    none


    Outputs:
    none
    """

    file_name = 'image_seconds_6780.bmp'

    image_raw = cv2.imread(path+file_name)

    image_raw = resize_image(image_raw,resize=16)

    image = preprocess_image(image_raw)

    #cv2.imwrite('../presentation_images/filtered_image.jpg', image) 

    #keypoints = detect_blobs(image)

    #wells_map = create_wells_map(image,keypoints)

    get_pixel_groupings(image,image_raw)#,keypoints,wells_map)

    cv2.destroyAllWindows()


def extract_features(img):
    """
    Extract features from image.
    NOTE: opencv uses BGR format

    Inputs:
    image = RGB image after applying mask to isolate one well

    Outputs:
    features = dictionary of features for the well in the image
    """

    #print(image.shape)
    #print(type(image))


    # Convert the BRG image to RGB
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Convert the RGB image to HSV
    img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    # Get image dimensions
    [h,w,c] = img.shape

    r_array = []
    g_array = []
    b_array = []
    h_array = []
    s_array = []
    v_array = []

    # Loop through array and get relevant pixel values
    for i in range(h):
        for j in range(w):

            # If pixel is in well
            if img[i,j,0] + img[i,j,1] + img[i,j,2] != 0:

                # Add to arrays to keep track of each channel
                r_array.append(img[i,j,0])
                g_array.append(img[i,j,1])
                b_array.append(img[i,j,2])
                h_array.append(img_hsv[i,j,0])
                s_array.append(img_hsv[i,j,1])
                v_array.append(img_hsv[i,j,2])

    # Calculate features
    r_avg = np.mean(r_array)
    g_avg = np.mean(g_array)
    b_avg = np.mean(b_array)
    h_avg = np.mean(h_array)
    s_avg = np.mean(s_array)
    v_avg = np.mean(v_array)

    #print(r_avg)
    #print(g_avg)
    #print(b_avg)

def create_well_mask(labels,group):
    """
    Create a mask to be applied to the color image to extract 

    Inputs:
    labels = numpy array output by cv2's connected components
    group = group to make the mask for (integer)

    Outputs:
    mask = binary numpy array
    """

    mask = (labels == group).astype(int)

    return mask


def get_pixel_groupings(img,img_raw):#,keypoints,wells_map):
    """
    Use the keypoints from openCV's blob detector and expand outward to get all relevant pixels per well

    Inputs:
    img = thresholded binary image 
    keypoints = tuple of keypoints returned by openCV blob detector

    Outputs:
    well_dict = dictionary of arrays, each array holds the coordinates for all pixels in a well
    """

    # Find connected pixels
    connectivity = 8
    output = cv2.connectedComponentsWithStats(img, connectivity, cv2.CV_32S)
    (numLabels, labels, stats, centroids) = output
    #print(output)
    #labels = 7*labels

    # Make a copy of array
    my_array = labels

    #print(centroids.shape)

    # Get location of well one
    well_loc = get_first_well_location(centroids)

    #print(well_loc)
    start = time.time()
    # Hard coded for 32 wells per image
    for i in range(32): 

        group = get_group_from_pixel(labels,well_loc)

        mask = create_well_mask(labels,group)

        # Convert 
        #labels[labels == group] = 255

        #labels = labels.astype(np.uint8)

        # Get next closest well_location
        [well_loc,centroids] = get_next_well(well_loc,centroids)

        # Apply mask to raw image
        res = cv2.bitwise_and(img_raw,img_raw,mask = mask.astype(np.uint8))

        #show_image(res)
        #print(f'Extracting features for well #{i+1}')
        extract_features(res)
        #print()
    end = time.time()

    print(f'Processing time for image is {round(end-start,3)} seconds')


def get_next_well(well_loc,centroids):
    """
    Remove current well and get location of next closest well
    """

    # First remove current well_loc
    index = np.where((centroids == well_loc).all(axis=1))[0][0]
    centroids = np.delete(centroids, (index),axis=0)

    min_dist = 99999
    next_well_index = None

    # Find next closest well
    for i,centroid in enumerate(centroids):

        # Calculate distance 
        dist = (centroid[0]-well_loc[0])**2+(centroid[1]-well_loc[1])**2

        # Save centroid if closest 
        if dist < min_dist:
            min_dist = dist
            next_well_index = i

    well_loc = centroids[next_well_index,:]

    return [well_loc,centroids]


def get_first_well_location(centroids):
    """
    Need to find the centroids with two biggest y values and then select the one with the biggest x value
    """

    #print(centroids)
    max_y_1 = [0,0]
    max_y_2 = [0,0]

    for centroid in centroids:

        if centroid[1] > max_y_1[1]:
            max_y_2 = max_y_1
            max_y_1 = centroid

        elif centroid[1] > max_y_2[1]:
            max_y_2 = centroid


    if max_y_1[0] > max_y_2[0]:

        return max_y_1

    else:
        
        return max_y_2


def get_group_from_pixel(image,pixel):
    """
    This will return the grouping number given a pixel

    Inputs:
    image = A segmented image output by cv2 connected components
    pixel = tuple of a pixel location

    Outputs:
    group = integer value of group
    """

    w = int(pixel[0])
    h = int(pixel[1])

    group = image[h,w]

    return group

def create_wells_map(image,keypoints):

    mapping_arr = [32,1,2,31,30,3,29,4,28,5,6,27,7,26,8,25,9,24,10,23,11,22,12,21,13,20,19,14,15,18,16,17]
    wells_map = {}

    img = np.copy(image)
    # Each keypoint represents the center of a well
    for i,keypoint in enumerate(keypoints):

        # Get height and width location of center pixels
        h = round(keypoint.pt[0])
        w = round(keypoint.pt[1])
        
        # Store center pixel to dict or array
        wells_map[mapping_arr[i]] = (w,h)

        #img[w,h] = 255
        #quit()

        #cv2.imshow('Testing centers', img)
        #cv2.waitKey(0)

    #print(well_dict)
    return wells_map


def detect_blobs(img):
    """
    Detect the blobs in the image to automatically define wells
    Helpful in getting blob detector working https://stackoverflow.com/questions/39083360/why-cant-i-do-blob-detection-on-this-binary-image

    Inputs:
    img = preprocessed image ready for blob detection

    Outputs:
    keypoints = tuple with blob locations
    """

    print("Setting up openCV detector")

    # Customize parameters for blob detector
    params = cv2.SimpleBlobDetector_Params()
    params.filterByCircularity = False
    params.filterByConvexity = False

    # print(params.filterByColor)
    # print(params.filterByArea)
    # print(params.filterByCircularity)
    # print(params.filterByInertia)
    # print(params.filterByConvexity)


    detector = cv2.SimpleBlobDetector_create(params)

    print("Attempting to detect wells")

    # Detect blobs
    keypoints = detector.detect(img)

    print("Drawing wells on image")
     
    # Draw detected blobs as red circles.
    # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
    im_with_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS) # Change (0,0,255) 
     
    # Show keypoints
    cv2.imshow("Keypoints", im_with_keypoints)
    cv2.waitKey(0)

    return keypoints


def preprocess_image(image,resize=False):
    """
    This function handles all the preprocessing for the image before well detection
    Blobs will have a pixel value of 0 and background will be 255
    
    Inputs:
    file_name = name of the image file to be processed

    Outputs:
    thresh = thresholded image after preprocessing has been performed
    """

    # Resize the image to a more reasonable size
    if resize:
        image = resize_image(image,resize=16)

    #show_image(image,resize=1)

    # Convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Apply median filter
    gray = cv2.medianBlur(gray,9)

    # Apply thresholding
    ret,thresh = cv2.threshold(gray,17,255,cv2.THRESH_BINARY)

    # Invert image for use in openCV functions
    #img = cv2.bitwise_not(thresh)

    return thresh


def resize_image(img,resize=2):
    """
    Helper function for displaying open cv image objects
    """
    try:
        [h,w] = img.shape
    except:
        [h,w,c] = img.shape

    # Calculate new dimensions
    h = int(h/resize)
    w = int(w/resize)

    # Resize image and display
    print(f"Image size before resizing {img.shape}")
    im = cv2.resize(img,(h,w))    
    print(f"Image size after resizing {im.shape}")               

    return im


def show_image(img):
    """
    Helper function for displaying open cv image objects
    """              
    cv2.imshow("output", img)                       
    cv2.waitKey(0)      


def plot_histogram(img):
    """ 
    This function will plot the histogram of an image for each channel
    """

    try:
        channels = img.shape[2]

        for i in range(channels):
            
            im = img[:,:,i]

            # Compute histogram
            hist, bin_edges = np.histogram(im, bins = range(256))

            # Plot histogram
            plt.bar(bin_edges[:-1], hist, width = 1)
            plt.xlim(min(bin_edges), max(bin_edges))
            plt.show()  


    except:

        # Compute histogram
        hist, bin_edges = np.histogram(img, bins = range(256))

        # Plot histogram
        plt.bar(bin_edges[:-1], hist, width = 1)
        plt.xlim(min(bin_edges), max(bin_edges))
        plt.show()  


def main_gpt(image):
    """
    Code from ChatGPT - can maybe use later or as an alternative
    """

    # Convert the image to grayscale
    #gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = image

    # Apply edge detection
    edges = cv2.Canny(gray, 50, 150)

    # Find contours
    contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Iterate over the contours and filter by area
    min_area = 100 # set the minimum area of the rectangle to detect
    for contour in contours:
        if cv2.contourArea(contour) < min_area:
            continue
            
        # Approximate the contour as a rectangle
        rect = cv2.minAreaRect(contour)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        
        # Draw the rectangle on the image
        cv2.drawContours(gray, [box], 0, (0, 255, 0), 3)

    #cv2.drawContours(image, contours, 3, (0,255,0), 3)

    # Show the image with rectangles
    cv2.imshow("Rectangles", gray)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def well_detection_demo():

    file_name = 'image_seconds_6780.bmp'

    image = cv2.imread(path+file_name)

    image = preprocess_image(image,resize=True)

    # Find connected pixels
    connectivity = 8
    #img_inv = cv2.bitwise_not(image)
    output = cv2.connectedComponentsWithStats(image, connectivity, cv2.CV_32S)
    (numLabels, labels, stats, centroids) = output
    #print(output)
    #labels = 7*labels

    # Make a copy of array
    my_array = labels

    print(centroids.shape)

    # Get location of well one
    well_loc = get_first_well_location(centroids)

    #print(well_loc)

    # Hard coded for 32 wells per image
    for i in range(32): 

        group = get_group_from_pixel(labels,well_loc)

        mask = create_well_mask(labels,group)

        # Convert 
        labels[labels == group] = 255

        labels = labels.astype(np.uint8)
        image = labels.astype(np.uint8)

        cv2.imshow('Testing centers', image)
        cv2.waitKey(0)

        #show_image((255*mask).astype(np.uint8))

        # Get next closest well_location
        [well_loc,centroids] = get_next_well(well_loc,centroids)

    cv2.destroyAllWindows()


# OLD CRAP
    # quit()

    # print(keypoints)

    # im_with_keypoints = cv2.drawKeypoints(image, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DEFAULT)#cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # cv2.imshow('Detected Blobs',im_with_keypoints)
    # cv2.waitKey(0)

    # img_blur=thresh1
     
    # # Canny Edge Detection
    # edges = cv2.Canny(image=img_blur, threshold1=100, threshold2=200) # Canny Edge Detection
    # # Display Canny Edge Detection Image
    # cv2.imshow('Canny Edge Detection', edges)
    # cv2.waitKey(0)

    # # Canny Edge Detection
    # edges = cv2.Canny(image=gray, threshold1=100, threshold2=200) # Canny Edge Detection
    # # Display Canny Edge Detection Image
    # cv2.imshow('Canny Edge Detection 2', edges)
    # cv2.waitKey(0)

    # cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
    #well_detection_demo()