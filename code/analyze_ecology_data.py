# Script for analyzing data
# Author: Matt Rochford (mrochfor@ucsc.edu)

# Import packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def main(file):

	df = pd.read_pickle(file)

	df['minutes'] = df.apply(lambda row: int(row.seconds/60), axis=1)

	#print(df.well.dtype)

	wells = [1,2,4,5,7,8]

	#df = df[df['well'].isin(wells)]
	print(df.shape)




	all_columns=['well','r_avg','r_max','r_min','g_avg','g_max','g_min','b_avg','b_max','b_min','h_avg','h_max','h_min','s_avg','s_max','s_min','v_avg','v_max','v_min','l_avg','l_max','l_min','a_avg','a_max','a_min','b0_avg','b0_max','b0_min']
	columns=['r_avg','r_max','g_avg','b_avg','h_avg','h_max','h_min','s_avg','s_min','v_avg','v_min','l_avg','l_max','l_min','a_avg','a_max','a_min','b0_avg','b0_max','b0_min']


	for c in columns:
		
		sns.lineplot(df['minutes'], df[c], hue=df['well'])
		plt.title(c)
		plt.show()

	# 	#input("Press Enter to continue...")

	# b0 max is best feature

	### Replicate level calculations
	df = df[['well', 'b0_max', 'seconds']]
	df = df.rename(columns={"b0_max": "b_max"})
	df['std'] = None
	#print(df.head(10))

	make_seaborn_plot(df)
	
	for i in range(1,33,2):

		# Get wells for replicate set
		wells = [i,i+1]

		# Get subset dataframe for wells
		df_subset = df.loc[df['well'].isin(wells)]
		#print(df_subset.head(10))

		# Calculate mean and std deviation at each time step
		for sec in df.seconds.unique():

			# Get values from dataframe
			values = df_subset[df_subset['seconds']==sec]['b_max'].values[:]
			
			# Calculate mean and std deviation
			m = np.mean(values,axis=0)
			sd = np.std(values,axis=0)

			# Add rows back to dataframe
			mean_row = ['replicate_'+str(i//2+1),m,sec,sd]
			#print(mean_row)
			#print(df.head(2))
			df.loc[len(df)] = mean_row

	make_plots(df)

def make_seaborn_plot(df):

	df['minutes'] = df['seconds']/60
	df['replicate_set'] = ''

	for i in range(1,33,2):

		# Get wells for replicate set
		wells = [i,i+1]

		# Get subset dataframe for wells
		df.loc[df['well'].isin(wells),'replicate_set'] = str(i//2+1)

	## Separate the dataframe into smaller dataframes
	#new_df = df[replicate_set = 1,2,3,4]
	#new_df2 = df[replicate_set =5,6,7,8]

	
	sns.lineplot(data=new_df, x="minutes", y="b_max", hue="replicate_set",palette='tab20c')
	plt.show()


def make_plots(df):
	#print(df.tail(10))
	#exit()
	df = df[~df['std'].isnull()]

	df['minutes'] = df['seconds']/60

	sns.lineplot(df['minutes'], df['b_max'], hue=df['well'],palette='tab20c')
	plt.title('B Max')
	plt.show()



	exit()

	### 1. point plot - Fluorescence vs time - with error bars being controlable confidence interval (i sent you this already) - option to take every X data point from set (sometimes may only wanna graph like every third for example) ###
	df_subset_1 = df.loc[df['well']=='replicate_1']
	plt.errorbar(df_subset_1['seconds'], df_subset_1['b_max'], df_subset_1['std'], linestyle='None', marker='^',label='Replicate #1')

	df_subset_2 = df.loc[df['well']=='replicate_2']
	plt.errorbar(df_subset_2['seconds'], df_subset_2['b_max'], df_subset_2['std'], linestyle='None', marker='^',label='Replicate #2')


	plt.legend()
	plt.xlabel('Seconds')
	plt.ylabel('B Max')
	plt.show()


if __name__ == '__main__':
	#file = "fluorescence_data.pkl"
	file = "../data/fluorescence_data_ecology.pkl"
	main(file)