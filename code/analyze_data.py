# Script for analyzing data
# Author: Matt Rochford (mrochfor@ucsc.edu)

# Import packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def main(file):

	df = pd.read_pickle(file)

	df['minutes'] = df.apply(lambda row: int(row.seconds/60), axis=1)

	#print(df.well.dtype)

	wells = [1,2,4,5,7,8]

	#df = df[df['well'].isin(wells)]
	print(df.shape)




	columns=['well','r_avg','r_max','r_min','g_avg','g_max','g_min','b_avg','b_max','b_min','h_avg','h_max','h_min','s_avg','s_max','s_min','v_avg','v_max','v_min','l_avg','l_max','l_min','a_avg','a_max','a_min','b0_avg','b0_max','b0_min']


	for c in columns:
		
		sns.lineplot(df['minutes'], df[c], hue=df['well'])
		plt.title(c)
		plt.show()

		#input("Press Enter to continue...")








if __name__ == '__main__':
	#file = "fluorescence_data.pkl"
	file = "../data/fluorescence_data_ecology.pkl"
	main(file)