# Main script for processing iamge data
# Author: Matt Rochford (mrochfor@ucsc.edu)

# Import packages
import pandas as pd
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
import cv2
#import queue
import time

# Wrapper modules
import glob

# Define paths to data for global use
path_NEB = "../data/NEB21_raise/"
path = "../data/"
path_fluorescence = "../data/fluorescence/09012022_5uM_50msEXP_75mil_1000_folddilRPAInput/"


def wrapper():

    # Use this code to get a list of all files in directory
    # Images range from 0 to 7140 with 60 second increments
    files = glob.glob(path_fluorescence+"*.bmp")
    files.sort()

    # Process files in order manually
    for i in range(0,7200,60):

        # This code is for checking for missing files
        # if path_fluorescence+"image_seconds_"+str(i)+".bmp" not in files:
        #     print(f"Missing image_seconds_"+str(i)+".bmp")

        # Define file name
        file = path_fluorescence+"image_seconds_"+str(i)+".bmp"
        print(f"Processing {file.split('/')[-1]}")

        # Process file
        if i == 0:
            df = main(file)
        else:
            df = main(file,df)

        # Save file
        df.to_pickle("fluorescence_data.pkl")
        df.to_csv("fluorescence_data.csv")


def main(file_name,df=None,resolution=16):
    """
    Main function for loading and processing data

    
    Inputs:
    none


    Outputs:
    df_merged = dataframe of features for each file
    """
    start = time.time()

    # Load from file and resize
    image_raw = cv2.imread(path+file_name)
    image_raw = resize_image(image_raw,resize=resolution)

    # Convert to other color spaces and preprocess image
    image_rgb = cv2.cvtColor(image_raw, cv2.COLOR_BGR2RGB) # Can save 55 ms here if we just leave in BGR space (assuming full resolution, significantly less in lower resolution)
    image_hsv = cv2.cvtColor(image_rgb, cv2.COLOR_RGB2HSV)
    image_lab = cv2.cvtColor(image_rgb, cv2.COLOR_RGB2LAB)
    image = preprocess_image(image_rgb)

    # Get pixel groupings
    connectivity = 8
    output = cv2.connectedComponentsWithStats(image, connectivity, cv2.CV_32S)
    (numLabels, labels, stats, centroids) = output

    # Get location of well one
    well_loc = get_first_well_location(centroids)

    # Initialize list to eventually turn into dataframe
    df_list = []

    # Hard coded for 32 wells per image
    for i in range(32): 

        # Determine well group from centroid pixel
        group = get_group_from_pixel(labels,well_loc)

        # Create mask for pixels from that group
        mask = create_well_mask(labels,group)

        # Get next closest well_location
        [well_loc,centroids] = get_next_well(well_loc,centroids)

        # Apply mask to images
        res_rgb = cv2.bitwise_and(image_rgb,image_rgb,mask = mask.astype(np.uint8))
        res_hsv = cv2.bitwise_and(image_hsv,image_hsv,mask = mask.astype(np.uint8))
        res_lab = cv2.bitwise_and(image_lab,image_lab,mask = mask.astype(np.uint8))

        # Extract features for each color space
        rgb_features = extract_features(res_rgb)
        hsv_features = extract_features(res_hsv)
        lab_features = extract_features(res_lab)

        # Create list of features for df row
        df_row = [i+1] + rgb_features + hsv_features +lab_features

        # Append row to list of lists
        df_list.append(df_row)

    # For storing in df
    file_name = file_name.split('/')[-1]

    # Create new dataframe for file
    df_new = pd.DataFrame(df_list,columns=['well','r_avg','r_max','r_min','g_avg','g_max','g_min','b_avg','b_max','b_min','h_avg','h_max','h_min','s_avg','s_max','s_min','v_avg','v_max','v_min','l_avg','l_max','l_min','a_avg','a_max','a_min','b0_avg','b0_max','b0_min'])
    df_new['resolution'] = resolution
    df_new['file_name'] = file_name
    df_new['seconds'] = int(file_name.split('_')[2].split('.')[0])

    # Append to existing dataframe
    if df is not None:
        df_merged = df.append(df_new, ignore_index=True)

    else:
        df_merged = df_new

    end = time.time()

    print(f'Processing time for image is {round(end-start,3)} seconds')

    cv2.destroyAllWindows()

    return df_merged


# Room for a lot of optimization here
def extract_features(img):
    """
    Extract features from image.
    NOTE: opencv uses BGR format

    Inputs:
    image = RGB image after applying mask to isolate one well

    Outputs:
    features = dictionary of features for the well in the image
    """

    # Get image dimensions
    [h,w,c] = img.shape

    # Using r, g, b monikers but could be any 3 channel color space
    r_array = []
    g_array = []
    b_array = []

    # Loop through array and get relevant pixel values
    for i in range(h):
        for j in range(w):

            # If pixel is in well
            if img[i,j,0] + img[i,j,1] + img[i,j,2] != 0:

                # Add to arrays to keep track of each channel
                r_array.append(img[i,j,0])
                g_array.append(img[i,j,1])
                b_array.append(img[i,j,2])

    # Calculate features
    r_avg = int(np.mean(r_array))
    g_avg = int(np.mean(g_array))
    b_avg = int(np.mean(b_array))

    r_max = int(max(r_array))
    g_max = int(max(g_array))
    b_max = int(max(b_array))

    r_min = int(min(r_array))
    g_min = int(min(r_array))
    b_min = int(min(r_array))

    return [r_avg,r_max,r_min,g_avg,g_max,g_min,b_avg,b_max,b_min]


def create_well_mask(labels,group):
    """
    Create a mask to be applied to the color image to extract 

    Inputs:
    labels = numpy array output by cv2's connected components
    group = group to make the mask for (integer)

    Outputs:
    mask = binary numpy array
    """

    mask = (labels == group).astype(int)

    return mask


def get_next_well(well_loc,centroids):
    """
    Remove current well and get location of next closest well
    """

    # First remove current well_loc
    index = np.where((centroids == well_loc).all(axis=1))[0][0]
    centroids = np.delete(centroids, (index),axis=0)

    min_dist = 9999999999
    next_well_index = None

    # Find next closest well
    for i,centroid in enumerate(centroids):

        # Calculate distance 
        dist = (centroid[0]-well_loc[0])**2+(centroid[1]-well_loc[1])**2

        # Save centroid if closest 
        if dist < min_dist:
            min_dist = dist
            next_well_index = i

    well_loc = centroids[next_well_index,:]

    return [well_loc,centroids]


def get_first_well_location(centroids):
    """
    Need to find the centroids with two biggest y values and then select the one with the biggest x value
    """

    #print(centroids)
    max_y_1 = [0,0]
    max_y_2 = [0,0]

    for centroid in centroids:

        if centroid[1] > max_y_1[1]:
            max_y_2 = max_y_1
            max_y_1 = centroid

        elif centroid[1] > max_y_2[1]:
            max_y_2 = centroid


    if max_y_1[0] > max_y_2[0]:

        return max_y_1

    else:
        
        return max_y_2


def get_group_from_pixel(image,pixel):
    """
    This will return the grouping number given a pixel

    Inputs:
    image = A segmented image output by cv2 connected components
    pixel = tuple of a pixel location

    Outputs:
    group = integer value of group
    """

    w = int(pixel[0])
    h = int(pixel[1])

    group = image[h,w]

    return group


def preprocess_image(image,resize=False):
    """
    This function handles all the preprocessing for the image before well detection
    Blobs will have a pixel value of 0 and background will be 255
    
    Inputs:
    file_name = name of the image file to be processed

    Outputs:
    thresh = thresholded image after preprocessing has been performed
    """

    # Resize the image to a more reasonable size
    if resize:
        image = resize_image(image,resize=16)

    #show_image(image,resize=1)

    # Convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Apply median filter
    gray = cv2.medianBlur(gray,9)

    # Apply thresholding
    ret,thresh = cv2.threshold(gray,17,255,cv2.THRESH_BINARY)

    # Invert image for use in openCV functions
    #img = cv2.bitwise_not(thresh)

    return thresh


def resize_image(img,resize=2):
    """
    Helper function for displaying open cv image objects
    """
    try:
        [h,w] = img.shape
    except:
        [h,w,c] = img.shape

    # Calculate new dimensions
    h = int(h/resize)
    w = int(w/resize)

    # Resize image and display
    print(f"Image size before resizing {img.shape}")
    im = cv2.resize(img,(h,w))    
    print(f"Image size after resizing {im.shape}")               

    return im


def show_image(img):
    """
    Helper function for displaying open cv image objects
    """       
    img = img.astype(np.uint8)       
    cv2.imshow("output", img)                       
    cv2.waitKey(0)      


if __name__ == '__main__':
    #main(file_name = 'image_seconds_6780.bmp')
    wrapper()