# Main script for processing iamge data
# Author: Matt Rochford (mrochfor@ucsc.edu)

# Import packages
import pandas as pd
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
import cv2



# Define path to data
path_NEB = "../data/NEB21_raise/"
path = "../data/"


def main():
	"""
	Main function for loading and processing data

	
	Inputs:
	none


	Outputs:
	none
	"""
	file_name = 'image_seconds_6780.bmp'

	#data0 = np.load(path+file_name)

	#print(f'Loading {file_name}')
	#print(f'Shape of loaded data: {data0.shape}') # (3472, 5792)

	data1 = np.array(Image.open(path+file_name))

	print(f'Loading {file_name}')
	print(f'Shape of loaded data: {data1.shape}') # (3472, 5792)

	# Visualize the image data
	#plt.imshow(data0, interpolation='nearest') # Numpy file data looks a little corrupted
	#plt.show()

	#plt.imshow(data1, interpolation='nearest') # bmp file data looks better
	#plt.show()

	# Plot the histogram for each channel of bmp image
	channels = ['R','G','B']
	for i in range(data1.shape[-1]):

		# R channel looks bad. G and B channels look good (and the same)
		print(f'Plotting {channels[i]} data')
		
		# Separate channel to its own image
		img = data1[:,:,i]
		
		# Compute histogram
		hist, bin_edges = np.histogram(img, bins = range(256))

		plot = True
		
		if plot:

			#print(img.shape)
			#print(f'Max pixel value is {img[np.unravel_index(img.argmax(),img.shape)]}')
			#print(f'Min pixel value is {img[np.unravel_index(img.argmin(),img.shape)]}')
			#print(img[img.argmin()])
		
			#plt.imshow(img)#, interpolation='nearest') # bmp file data looks better
			plt.imshow(img, cmap='gray', vmin=0, vmax=255)
			plt.show()

			plt.bar(bin_edges[:-1], hist, width = 1)
			plt.xlim(min(bin_edges), max(bin_edges))
			plt.show()   

		#print(bin_edges)


		# Do edge detection
		# img_blur = cv2.GaussianBlur(img,(3,3),sigmaX=0, sigmaY=0) 

		# # Sobel Edge Detection
		# sobelx = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=0, ksize=5) # Sobel Edge Detection on the X axis
		# sobely = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=0, dy=1, ksize=5) # Sobel Edge Detection on the Y axis
		# sobelxy = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=1, ksize=5) # Combined X and Y Sobel Edge Detection
		 
		# # Display Sobel Edge Detection Images
		# cv2.imshow('Sobel X', sobelx)
		# cv2.waitKey(0)
		 
		# cv2.imshow('Sobel Y', sobely)
		# cv2.waitKey(0)
		 
		# cv2.imshow('Sobel X Y using Sobel() function', sobelxy)
		# cv2.waitKey(0)

if __name__ == '__main__':
	main()