# Main script for plotting translucence vs time
# Author: Matt Rochford (mrochfor@ucsc.edu)

# Import packages
import pandas as pd
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
import cv2
import math
import seaborn as sns
sns.set()



# Define path to data
path = "../data/"


def load_file(file_name,table=False):
    """
    Function for loading files into pandas dataframes or numpy arrays

    
    Inputs:
    file_name = name of file to be loaded
    table = if data is a table will load using pandas, otherwise use numpy


    Outputs:
    data = either a pandas dataframe or numpy array
    """

    if table:

        print(f"Loading {file_name} into a pandas dataframe")
        data = pd.read_csv(path+file_name)
        print(f"Dataframe has a shape of {data.shape}")

    else:

        print(f"Loading {file_name} into a numpy array")
        data = np.load(path+file_name)
        print(f"Array has a shape of {data.shape}")

    return data


def calculate_fluorescence(df):
    """
    This function performs the calculations for fluorescence

    Inputs:
    df = dataframe that contains SYBR and ROX

    Outputs:
    df = input dataframe with an added column for fluorescence
    """

    # Convert string values to float
    df['SYBR'] = df['SYBR'].str.replace(',','').astype('float64')
    df['ROX'] = df['ROX'].str.replace(',','').astype('float64')
    
    # Divide SYBR by ROX
    df['Fluorescence'] = df.SYBR / df.ROX

    # Bring lowest fluorescence to zero and apply background subtraction
    df['Fluorescence'] = df['Fluorescence'] - df['Fluorescence'].min()
    #print(df.head(10))

    return df


def plot_fluorescence(df):
    """

    """
    plt.plot('Cycle','Fluorescence',data=data)#[['Cycle','Fluorescence']])
    plt.xlabel('Cycle')
    plt.ylabel('Fluorescence')
    plt.title(f'Fluorescence Over Time for Well {well_no}')
    plt.show()


def main():
    """
    Main function for loading and processing data

    
    Inputs:
    none


    Outputs:
    none
    """

    # Load data
    df = load_file('translucent_data_0.csv',table=True)

    # Remove rows without data
    df = df[df['SYBR'].notnull()]

    # Determine active wells
    well_min = df['Well'].min()
    well_max = df['Well'].max()

    # Create dictionary to hold dataframes for each well
    well_data = {}

    for well_no in range(well_min,well_max):

        # Split dataframe by well position
        df_new = df.loc[(df['Well'] == well_no)].reset_index()

        # Store filtered dataframe in dict
        well_data[well_no] = df_new


    # Make plot for each dataframe
    fig,axs = plt.subplots(4,2)
    for well_no in range(well_min,well_max):

        data = well_data[well_no]

        data = calculate_fluorescence(data)

        axs[math.floor(well_no/2-1), well_no%2].plot('Cycle','Fluorescence',data=data)
        axs[math.floor(well_no/2-1), well_no%2].set_title(f'Fluorescence Over Time for Well {well_no}')

        # plt.plot('Cycle','Fluorescence',data=data)#[['Cycle','Fluorescence']])
        # plt.xlabel('Cycle')
        # plt.ylabel('Fluorescence')
        # plt.title(f'Fluorescence Over Time for Well {well_no}')
        # plt.show()

    for ax in axs[3,:]:
        ax.set(xlabel='Cycle')

    for ax in axs[:3,:].flat:
        plt.setp(ax.get_xticklabels(), visible=False)

    for ax in axs.flat:
        #plt.setp(ax.get_xticklabels(), visible=False)
        ax.set(ylabel='Fluorescence')

    plt.show()



if __name__ == '__main__':
    main()